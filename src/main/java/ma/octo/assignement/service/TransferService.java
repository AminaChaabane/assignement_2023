package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TransferService {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

    private final CompteService compteService;

    private final TransferRepository transferRepository;
    private final AuditService auditService;

    @Autowired
    public TransferService(CompteService compteService, TransferRepository transferRepository, AuditService auditService) {
        this.compteService = compteService;
        this.transferRepository = transferRepository;
        this.auditService = auditService;
    }

    public List<Transfer> getAllTransfers() {
        LOGGER.info("Lister les transferts");
        var allTransfers = transferRepository.findAll();
        return CollectionUtils.isEmpty(allTransfers) ? null : allTransfers;
    }

    public void createTransaction(TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Compte compteEmetteur = compteService.checkCompteExistsByNr(transferDto.getNrCompteEmetteur(), "Compte Emetteur Non existant");
        Compte compteRecepteur = compteService.checkCompteExistsByNr(transferDto.getNrCompteBeneficiaire(), "Compte Beneficiaire Non existant");

        if (transferDto.getMontant() == null || transferDto.getMontant().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().intValue() < 10) {
            System.out.println("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif() == null || transferDto.getMotif().length() ==0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compteEmetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }

        compteService.updateSolde(compteEmetteur, compteEmetteur.getSolde().subtract(transferDto.getMontant()));

        compteService.updateSolde(compteRecepteur, new BigDecimal(compteRecepteur.getSolde().intValue() + transferDto.getMontant().intValue()));

        Transfer transfer = TransferMapper.map(transferDto);
        transfer.setCompteBeneficiaire(compteRecepteur);
        transfer.setCompteEmetteur(compteEmetteur);

        transferRepository.save(transfer);

        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());
    }


    public void save(Transfer transfer) {
        transferRepository.save(transfer);
    }
}
