package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositRepository;
import org.apache.catalina.mapper.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MoneyDepositService {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(MoneyDepositService.class);

    private final CompteService compteService;

    private final MoneyDepositRepository moneyDepositRepository;

    private final AuditService auditService;

    @Autowired
    public MoneyDepositService(CompteService compteService, MoneyDepositRepository moneyDepositRepository, AuditService auditService) {
        this.compteService = compteService;
        this.moneyDepositRepository = moneyDepositRepository;
        this.auditService = auditService;
    }

    public List<MoneyDeposit> getAllDeposits(){
        LOGGER.info("Lister les deposits");
        var allDeposits = moneyDepositRepository.findAll();
        return CollectionUtils.isEmpty(allDeposits) ? null : allDeposits;
    }

    public void createDeposit(MoneyDepositDto moneyDepositDto) throws CompteNonExistantException, TransactionException {

        Compte compteRecepteur = compteService.checkCompteExistsByRib(moneyDepositDto.getRib(), "Compte Beneficiaire Non existant");

        String nom_prenom_emetteur = moneyDepositDto.getNom_prenom_emetteur();

        if (nom_prenom_emetteur == null || nom_prenom_emetteur.isEmpty()){
            System.out.println("Emetteur vide");
            throw new TransactionException("Emetteur vide");
        }

        if (moneyDepositDto.getMontant() == null || moneyDepositDto.getMontant().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (moneyDepositDto.getMontant().intValue() < 10) {
            System.out.println("Montant minimal de deposit non atteint");
            throw new TransactionException("Montant minimal de deposit non atteint");
        } else if (moneyDepositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de deposit dépassé");
            throw new TransactionException("Montant maximal de deposit dépassé");
        }

        if (moneyDepositDto.getMotif() == null || moneyDepositDto.getMotif().length() ==0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        compteService.updateSolde(compteRecepteur, new BigDecimal(compteRecepteur.getSolde().intValue() + moneyDepositDto.getMontant().intValue()));

        MoneyDeposit moneyDeposit = DepositMapper.map(moneyDepositDto);
        moneyDeposit.setCompteBeneficiaire(compteRecepteur);

        moneyDepositRepository.save(moneyDeposit);

        auditService.auditDeposit("Deposit de la part de " + moneyDepositDto.getNom_prenom_emetteur() + " au profit de " + moneyDepositDto
                .getRib() + " d'un montant de " + moneyDepositDto.getMontant()
                .toString());

    }


    public void save(MoneyDeposit moneyDeposit) {
        moneyDepositRepository.save(moneyDeposit);
    }

}
