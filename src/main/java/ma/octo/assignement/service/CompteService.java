package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class CompteService {

    private final CompteRepository compteRepository;

    @Autowired
    public CompteService(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    public List<Compte> getAllAccounts() {
        List<Compte> allAccounts = compteRepository.findAll();
        return CollectionUtils.isEmpty(allAccounts) ? null : allAccounts;
    }

    public Compte checkCompteExistsByRib(String rib, String message) throws CompteNonExistantException {
        Compte compte = compteRepository.findByRib(rib);
        if(compte == null){
            System.out.println(message);
            throw new CompteNonExistantException(message);
        }

        return compte;
    }

    public Compte checkCompteExistsByNr(String nr, String message) throws CompteNonExistantException {
        Compte compte = compteRepository.findByNrCompte(nr);
        if(compte == null){
            System.out.println(message);
            throw new CompteNonExistantException(message);
        }

        return compte;
    }

    public void updateSolde(Compte compte, BigDecimal newSolde){
        compte.setSolde(newSolde);
        compteRepository.save(compte);
    }
}
