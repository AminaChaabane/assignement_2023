package ma.octo.assignement.web;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.MoneyDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/deposit")
public class MoneyDepositController {

    private MoneyDepositService moneyDepositService;

    @Autowired
    public MoneyDepositController(MoneyDepositService moneyDepositService) {
        this.moneyDepositService = moneyDepositService;
    }

    @GetMapping("listDesDeposits")
    List<MoneyDeposit> loadAllDeposits() {
        return moneyDepositService.getAllDeposits();
    }

    @PostMapping("/executerDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody MoneyDepositDto moneyDepositDto) throws TransactionException, CompteNonExistantException {
        moneyDepositService.createDeposit(moneyDepositDto);
    }

    @PostMapping("/saveDeposit")
    private void saveDeposit(MoneyDeposit moneyDeposit) {
        moneyDepositService.save(moneyDeposit);
    }
}
