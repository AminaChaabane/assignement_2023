package ma.octo.assignement.web;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping(path = "/transfers")
class TransferController {

    private TransferService transferService;

    @Autowired
    TransferController(TransferService transferService) {
        this.transferService = transferService;
    }


    @GetMapping("listDesTransfers")
    List<Transfer> loadAllTransfers() {
        return transferService.getAllTransfers();
    }


    @PostMapping("/executerTransfer")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        transferService.createTransaction(transferDto);
    }

    @PostMapping("/saveTransfer")
    private void saveTransfer(Transfer transfer) {
        transferService.save(transfer);
    }
}
