package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;

import java.util.Date;

public class DepositMapper {

    public static MoneyDepositDto map(MoneyDeposit moneyDeposit){
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setNom_prenom_emetteur(moneyDeposit.getNom_prenom_emetteur());
        moneyDepositDto.setRib(moneyDeposit.getCompteBeneficiaire().getRib());
        moneyDepositDto.setMotif(moneyDeposit.getMotifDeposit());
        moneyDepositDto.setMontant(moneyDeposit.getMontant());
        moneyDepositDto.setDate(moneyDeposit.getDateExecution());

        return moneyDepositDto;
    }

    public static MoneyDeposit map(MoneyDepositDto moneyDepositDto){
        MoneyDeposit moneyDeposit = new MoneyDeposit();
        moneyDeposit.setNom_prenom_emetteur(moneyDepositDto.getNom_prenom_emetteur());
        moneyDeposit.setMotifDeposit(moneyDepositDto.getMotif());
        moneyDeposit.setMontant(moneyDepositDto.getMontant());
        moneyDeposit.setDateExecution(moneyDepositDto.getDate() == null ? new Date() : moneyDepositDto.getDate());

        return moneyDeposit;
    }
}
