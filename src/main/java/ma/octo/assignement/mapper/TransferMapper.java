package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;

import java.util.Date;

public class TransferMapper {

    public static TransferDto map(Transfer transfer) {
        TransferDto transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        transferDto.setMontant(transfer.getMontantTransfer());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransfer());

        return transferDto;

    }

    public static Transfer map(TransferDto transferDto){
        Transfer transfer = new Transfer();
        transfer.setMontantTransfer(transferDto.getMontant());
        transfer.setMotifTransfer(transferDto.getMotif());
        transfer.setDateExecution(transferDto.getDate() == null ? new Date() : transferDto.getDate());

        return transfer;
    }
}
