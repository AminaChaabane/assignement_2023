package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditDepositRepository;
import ma.octo.assignement.repository.AuditTransferRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.actuate.endpoint.annotation.EndpointExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AuditServiceTest {

    @Mock
    private AuditTransferRepository auditTransferRepository;
    @Mock
    private AuditDepositRepository auditDepositRepository;
    private AuditService underTest;

    String message = "test";

    @BeforeEach
    void setUp() {
        underTest = new AuditService(auditTransferRepository, auditDepositRepository);
    }

    @Test
    void canAuditTransfer() {
        //Given
        AuditTransfer auditTransfer = new AuditTransfer(message, EventType.TRANSFER);

        //When
        underTest.auditTransfer(message);

        //Then
        ArgumentCaptor<AuditTransfer> transferArgumentCaptor = ArgumentCaptor.forClass(AuditTransfer.class);

        verify(auditTransferRepository).save(transferArgumentCaptor.capture());

        AuditTransfer captorAuditTransfer = transferArgumentCaptor.getValue();

        assertThat(captorAuditTransfer)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(auditTransfer);

    }

    @Test
    void canAuditDeposit() {
        //Given
        AuditDeposit auditDeposit = new AuditDeposit(message, EventType.DEPOSIT);

        //When
        underTest.auditDeposit(message);

        //Then
        ArgumentCaptor<AuditDeposit> depositArgumentCaptor = ArgumentCaptor.forClass(AuditDeposit.class);

        verify(auditDepositRepository).save(depositArgumentCaptor.capture());

        AuditDeposit captorAuditDeposit = depositArgumentCaptor.getValue();

        assertThat(captorAuditDeposit)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(auditDeposit);
    }
}