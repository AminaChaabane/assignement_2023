package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CompteServiceTest {

    @Mock
    CompteRepository compteRepository;
    CompteService underTest;

    Compte compte;

    String message = "test";

    @BeforeEach
    void setUp() {
        underTest = new CompteService(compteRepository);

        compte = new Compte();

        compte.setNrCompte("010000A000001000");
        compte.setRib("RIB1");
        compte.setSolde(BigDecimal.valueOf(200000L));
    }

    @Test
    void canGetAllAccounts() {
        //When
        underTest.getAllAccounts();
        //Then
        verify(compteRepository).findAll();
    }

    @Test
    void canCheckCompteExistsByRib() throws CompteNonExistantException {

        //Given
        given(compteRepository.findByRib(anyString())).willReturn(compte);

        //When
        underTest.checkCompteExistsByRib(compte.getRib(), message);

        //Then

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(compteRepository).findByRib(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue()).isEqualTo(compte.getRib());

    }

    @Test
    void willThrowWhenCompteDoesNotExistByRib(){
        //Given
        given(compteRepository.findByRib(anyString())).willReturn(null);

        //Then
        //When
        assertThatThrownBy(() -> underTest.checkCompteExistsByRib(compte.getRib(), message))
                .isInstanceOf(CompteNonExistantException.class)
                .hasMessageContaining(message);

    }

    @Test
    void canCheckCompteExistsByNr() throws CompteNonExistantException {

        //Given
        given(compteRepository.findByNrCompte(anyString())).willReturn(compte);

        //When
        underTest.checkCompteExistsByNr(compte.getNrCompte(), message);

        //Then

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(compteRepository).findByNrCompte(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue()).isEqualTo(compte.getNrCompte());

    }

    @Test
    void willThrowWhenCompteDoesNotExistByNr(){
        //Given
        given(compteRepository.findByNrCompte(anyString())).willReturn(null);

        //Then
        //When
        assertThatThrownBy(() -> underTest.checkCompteExistsByNr(compte.getNrCompte(), message))
                .isInstanceOf(CompteNonExistantException.class)
                .hasMessageContaining(message);

    }

    @Test
    void canUpdateSolde(){
        //Given
         BigDecimal newSolde = BigDecimal.valueOf(199900L);


        //When
        underTest.updateSolde(compte, newSolde);

        //Then
        ArgumentCaptor<Compte> argumentCaptor = ArgumentCaptor.forClass(Compte.class);
        verify(compteRepository).save(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().getSolde()).isEqualTo(newSolde);

    }

}