package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MoneyDepositServiceTest {

    @Mock
    CompteService compteService;

    @Mock
    MoneyDepositRepository moneyDepositRepository;

    @Mock
    AuditService auditService;

    MoneyDepositService underTest;

    Compte compteRecepteur;
    MoneyDeposit moneyDeposit;
    MoneyDepositDto moneyDepositDto;

    String message = "Compte Beneficiaire Non existant";

    @BeforeEach
    void setUp() {
        underTest = new MoneyDepositService(compteService, moneyDepositRepository, auditService);

        compteRecepteur = new Compte();
        compteRecepteur.setNrCompte("010000A000001000");
        compteRecepteur.setRib("RIB1");
        compteRecepteur.setSolde(BigDecimal.valueOf(200000L));

        moneyDeposit = new MoneyDeposit();
        moneyDeposit.setNom_prenom_emetteur("CHAABANE AMINA");
        moneyDeposit.setCompteBeneficiaire(compteRecepteur);
        moneyDeposit.setDateExecution(new Date());
        moneyDeposit.setMontant(BigDecimal.valueOf(1000L));
        moneyDeposit.setMotifDeposit("motif");

        moneyDepositDto = DepositMapper.map(moneyDeposit);
    }

    @Test
    void canGetAllDeposits() {
        //When
        underTest.getAllDeposits();
        //Then
        verify(moneyDepositRepository).findAll();
    }

    @Test
    void canCreateDeposit() throws TransactionException, CompteNonExistantException {
        //Given

        given(compteService.checkCompteExistsByRib(moneyDepositDto.getRib(), message)).willReturn(compteRecepteur);

        //When
        underTest.createDeposit(moneyDepositDto);

        //Then
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        verify(compteService).checkCompteExistsByRib(argumentCaptor.capture(), messageCaptor.capture());

        assertThat(argumentCaptor.getValue()).isEqualTo(compteRecepteur.getRib());
        assertThat(messageCaptor.getValue()).isEqualTo(message);
    }

    @Test
    void willThrowWhenEmetteurIsEmpty()  {
        //Given

        moneyDepositDto.setNom_prenom_emetteur(null);

        try {
            given(compteService.checkCompteExistsByRib(moneyDepositDto.getRib(), message)).willReturn(compteRecepteur);
        } catch (CompteNonExistantException e) {

        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createDeposit(moneyDepositDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Emetteur vide");
    }

    @Test
    void willThrowWhenMontantIsEmpty(){
        //Given

        moneyDepositDto.setMontant(BigDecimal.ZERO);

        try {
            given(compteService.checkCompteExistsByRib(moneyDepositDto.getRib(), message)).willReturn(compteRecepteur);
        } catch (CompteNonExistantException e) {

        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createDeposit(moneyDepositDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant vide");
    }

    @Test
    void willThrowWhenMontantLessThanTen(){
        //Given

        moneyDepositDto.setMontant(BigDecimal.ONE);

        try {
            given(compteService.checkCompteExistsByRib(moneyDepositDto.getRib(), message)).willReturn(compteRecepteur);
        } catch (CompteNonExistantException e) {

        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createDeposit(moneyDepositDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant minimal de deposit non atteint");
    }

    @Test
    void willThrowWhenMontantGreaterThanMax(){
        //Given

        moneyDepositDto.setMontant(BigDecimal.valueOf(10001L));

        try {
            given(compteService.checkCompteExistsByRib(moneyDepositDto.getRib(), message)).willReturn(compteRecepteur);
        } catch (CompteNonExistantException e) {

        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createDeposit(moneyDepositDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant maximal de deposit dépassé");
    }

    @Test
    void willThrowWhenMotifIsEmpty(){
        //Given

        moneyDepositDto.setMotif("");

        try {
            given(compteService.checkCompteExistsByRib(moneyDepositDto.getRib(), message)).willReturn(compteRecepteur);
        } catch (CompteNonExistantException e) {

        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createDeposit(moneyDepositDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Motif vide");
    }

    @Test
    void canSaveDeposit() {
        //When
        underTest.save(moneyDeposit);

        //Then
        ArgumentCaptor<MoneyDeposit> moneyDepositArgumentCaptor = ArgumentCaptor.forClass(MoneyDeposit.class);

        verify(moneyDepositRepository).save(moneyDepositArgumentCaptor.capture());

        assertThat(moneyDepositArgumentCaptor.getValue())
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(moneyDeposit);
    }
}