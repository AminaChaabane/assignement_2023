package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TransferServiceTest {

    @Mock
    CompteService compteService;

    @Mock
    TransferRepository transferRepository;

    @Mock
    AuditService auditService;

    TransferService underTest;

    Compte compte1;
    Compte compte2;
    Transfer transfer;
    TransferDto transferDto;

    String message1 = "Compte Emetteur Non existant";
    String message2 = "Compte Beneficiaire Non existant";

    @BeforeEach
    void setUp() {
        underTest = new TransferService(compteService, transferRepository, auditService);

        compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));

        compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));

        transfer = new Transfer();
        transfer.setMontantTransfer(BigDecimal.TEN);
        transfer.setCompteEmetteur(compte1);
        transfer.setCompteBeneficiaire(compte2);
        transfer.setMotifTransfer("motif");
        transfer.setDateExecution(new Date());

        transferDto = TransferMapper.map(transfer);


    }

    @Test
    void canGetAllTransfers() {
        //When
        underTest.getAllTransfers();
        //Then
        verify(transferRepository).findAll();
    }

    @Test
    void canCreateTransaction() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        //Given

        given(compteService.checkCompteExistsByNr(transferDto.getNrCompteEmetteur(), message1)).willReturn(compte1);
        given(compteService.checkCompteExistsByNr(transferDto.getNrCompteBeneficiaire(), message2)).willReturn(compte2);

        //When
        underTest.createTransaction(transferDto);

        //Then
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        verify(compteService, times(2)).checkCompteExistsByNr(argumentCaptor.capture(), messageCaptor.capture());

        assertThat(argumentCaptor.getAllValues().get(0)).isEqualTo(compte1.getNrCompte());
        assertThat(argumentCaptor.getAllValues().get(1)).isEqualTo(compte2.getNrCompte());
        assertThat(messageCaptor.getAllValues().get(0)).isEqualTo(message1);
        assertThat(messageCaptor.getAllValues().get(1)).isEqualTo(message2);
    }


    @Test
    void willThrowWhenMontantIsEmpty(){
        //Given

        transferDto.setMontant(BigDecimal.ZERO);

        try {
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteEmetteur(), message1)).willReturn(compte1);
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteBeneficiaire(), message2)).willReturn(compte2);
        } catch (CompteNonExistantException e) {
        }


        //When
        //Then
        assertThatThrownBy(() -> underTest.createTransaction(transferDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant vide");
    }

    @Test
    void willThrowWhenMontantLessThanTen(){
        //Given

        transferDto.setMontant(BigDecimal.ONE);

        try {
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteEmetteur(), message1)).willReturn(compte1);
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteBeneficiaire(), message2)).willReturn(compte2);
        } catch (CompteNonExistantException e) {
        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createTransaction(transferDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant minimal de transfer non atteint");
    }

    @Test
    void willThrowWhenMontantGreaterThanMax(){
        //Given

        transferDto.setMontant(BigDecimal.valueOf(10001L));

        try {
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteEmetteur(), message1)).willReturn(compte1);
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteBeneficiaire(), message2)).willReturn(compte2);
        } catch (CompteNonExistantException e) {
        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createTransaction(transferDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant maximal de transfer dépassé");
    }

    @Test
    void willThrowWhenMotifIsEmpty(){
        //Given

        transferDto.setMotif("");

        try {
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteEmetteur(), message1)).willReturn(compte1);
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteBeneficiaire(), message2)).willReturn(compte2);
        } catch (CompteNonExistantException e) {
        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createTransaction(transferDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Motif vide");
    }

    @Test
    void willThrowSoldeNotEnough(){
        //Given

        compte1.setSolde(BigDecimal.valueOf(1000L));

        transferDto.setMontant(BigDecimal.valueOf(2000L));

        try {
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteEmetteur(), message1)).willReturn(compte1);
            given(compteService.checkCompteExistsByNr(transferDto.getNrCompteBeneficiaire(), message2)).willReturn(compte2);
        } catch (CompteNonExistantException e) {
        }

        //When
        //Then
        assertThatThrownBy(() -> underTest.createTransaction(transferDto))
                .isInstanceOf(SoldeDisponibleInsuffisantException.class)
                .hasMessageContaining("Solde insuffisant pour l'utilisateur");
    }

    @Test
    void canSaveTransfer() {
        //When
        underTest.save(transfer);

        //Then
        ArgumentCaptor<Transfer> transferArgumentCaptor = ArgumentCaptor.forClass(Transfer.class);

        verify(transferRepository).save(transferArgumentCaptor.capture());

        assertThat(transferArgumentCaptor.getValue())
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(transfer);

    }
}