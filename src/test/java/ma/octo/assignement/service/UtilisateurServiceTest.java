package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UtilisateurServiceTest {

    @Mock
    UtilisateurRepository utilisateurRepository;

    UtilisateurService underTest;

    @BeforeEach
    void setUp() {
        underTest = new UtilisateurService(utilisateurRepository);
    }

    @Test
    void canGetAllUsers(){
        //When
        underTest.getAllUsers();
        //Then
        verify(utilisateurRepository).findAll();
    }
}